﻿
namespace Domain.Entity
{
    public class OrderItem
    {
        public Guid Id { get; set; }
        public required string Name { get; set; }
        public required decimal Quantity { get; set; }
        public required string Unit { get; set; }
    }
}
