﻿
namespace Domain.Entity
{
    public class Provider
    {
        public int Id { get; set; }
        public DateTime Created { get; set; } = DateTime.UtcNow;
        public required string Name { get; set; }    
        public List<Order> Orders { get; set; } = new List<Order>();
    }
}
