using Application.Services.AutoMapper;
using Infrastructure.Data;
using Microsoft.CodeAnalysis.Options;
using Microsoft.EntityFrameworkCore;
using NuGet.Packaging.Signing;
using Serilog;
using Web.Extensions;

namespace Web
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);
           
            // Registering and configuring the logging library
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.File("logs/myapp.txt", rollingInterval: RollingInterval.Day, retainedFileCountLimit: 2)
                .CreateLogger();

            // Extension method for registering third party services
            builder.Services.RegisterServices();
            // Database context factory registration
            builder.Services.AddDbContextFactory<OrderHandlerDbContext>(
            options => options.UseSqlServer(builder.Configuration.GetConnectionString("ForDB")));
                //options => options.UseInMemoryDatabase("inmemory-db"));
            //  Option registration extension method
            builder.Services.RegisterOptions(builder);
            builder.Services.AddControllersWithViews()
                    .AddSessionStateTempDataProvider();
            builder.Services.AddSession();

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (!app.Environment.IsDevelopment())
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
          
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();
            app.UseSession();

            app.MapControllerRoute(
                name: "default",
                pattern: "{controller=Orders}/{action=Index}/{id?}");

            var scope = app.Services.CreateScope();
            scope.ServiceProvider.GetRequiredService<OrderHandlerDbContext>().Database.EnsureCreated();


            app.Run();
        }
    }
}