﻿using Application.Models;

namespace Web.Models
{
    public class CreateOrderViewModel
    {
        public required List<string> AllProvidersName { get; set; }
        public required string SelectedProviderName { get; set; }
        public OrderModel Order { get; set; } = new() { Items = new () };
    }
}
