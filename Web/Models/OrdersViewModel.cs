﻿using Application.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Web.Models
{
    public class OrdersViewModel
    {
        public DateTime StartDate { get; set; } = DateTime.UtcNow.AddMonths(-1);
        public DateTime EndDate { get; set; } = DateTime.UtcNow;
        public List<int> SelectedProviderIds { get; set; } = new();
        public List<string> SelectedNumbers { get; set; } = new();
        public List<string> Numbers { get; set; } = new();
        public List<SelectListItem> Providers { get; set; } = new();
        public List<OrderModel> Orders { get; set; } = new();
    }
}
