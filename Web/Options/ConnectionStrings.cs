﻿using Web.Abstract;

namespace Web.Options
{
    public class ConnectionStrings : AppOptions
    {
        public string ForDB { get; set; } = string.Empty;

        override public Dictionary<string, string> GetMainOptions()
        {
            var options = new Dictionary<string, string>
            {
                { nameof(ForDB), ForDB },
            };
            return options;
        }

    }
}
