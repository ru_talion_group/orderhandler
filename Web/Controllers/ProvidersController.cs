﻿using Application.Features.Providers.Commands;
using Application.Features.Providers.Queries;
using Application.Models;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Web.Controllers
{
    public class ProvidersController : Controller
    {
        private readonly IMediator _mediator;
        public ProvidersController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var allProviders = await _mediator.Send(new GetAllProvidersQuery());
            return View(allProviders);
        }
        [HttpPost]
        public async Task<IActionResult> Create(string providerName)
        {
            if (providerName == null)
            {
                return RedirectToAction(nameof(Index));
            }
            await _mediator.Send(new AddProviderCommand() { ProviderName = providerName});
            return RedirectToAction(nameof(Index));
        }
        
        public async Task<IActionResult> Delete(int id)
        {
            await _mediator.Send(new DeleteProviderCommand() { ProviderId = id });
            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            return View(await _mediator.Send(new GetProviderByIdQuery() { ProviderId = id }));
        }

        [HttpPost]
        public async Task<IActionResult> Edit(ProviderModel provider)
        {
            await _mediator.Send(new EditProviderCommand() { Provider = provider});
            return RedirectToAction(nameof(Index));
        }
    }
}
