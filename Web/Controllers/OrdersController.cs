﻿using Application.Features.Orders.Commands;
using Application.Features.Orders.Queries;
using Application.Features.Providers.Queries;
using Application.Models;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Web.Extensions;
using Web.Models;

namespace Web.Controllers
{
    public class OrdersController : Controller
    {
        private readonly IMediator _mediator;
        public OrdersController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IActionResult> Index(OrdersViewModel model)
        {
            var providers = await _mediator.Send(new GetAllProvidersQuery());
            model.Providers.Add(new() { Value = null, Text = "All Providers" });
            model.Providers.AddRange
                (providers
                .Select(p => new SelectListItem { Value = p.Id.ToString(), Text = p.Name })
                .ToList());

            var allOrders = await _mediator.Send(new GetOrdersQuery() { PeriodStart = model.StartDate, PeriodEnd = model.EndDate }); 

            model.Numbers = allOrders.Select(o => o.Number).Distinct().ToList();

            var orders = allOrders.Where(x => x.Created >= model.StartDate & x.Created <= model.EndDate);

            if (model.SelectedProviderIds.Any())
            {
                orders = orders.Where(o => model.SelectedProviderIds.Contains(o.Provider.Id));
            }

            if (model.SelectedNumbers.Any(n => !string.IsNullOrWhiteSpace(n)))
            {
                orders = orders.Where(o => model.SelectedNumbers.Contains(o.Number));
            }

            model.Orders = orders.ToList();

            return View(model);
        }

        public async Task<IActionResult> Details(int id)
        {
            var order = await _mediator.Send(new GetOrderQuery() { OrderId = id });

            return View(order);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            var order = await _mediator.Send(new GetOrderQuery() { OrderId = id });

            return View(order);

        }

        [HttpPost, ActionName("Edit")]
        public async Task<IActionResult> EditPost([FromQuery]int Id, [FromBody] OrderModel orderModel)
        {
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Create(CreateOrderViewModel model)
        {
            var allProviders = await _mediator.Send(new GetAllProvidersQuery());
            model.AllProvidersName = allProviders.Select(x => x.Name).Distinct().ToList();

            var items = TempData.Get<List<OrderItemModel>>("OrderItems") ?? new();
            model.Order.Items = items;
            TempData.Set("OrderItems", items);
            
            return View(model);
        }

        [HttpPost, ActionName("Create")]
        public async Task<IActionResult> CreatePost(CreateOrderViewModel model)
        {
                var items = TempData.Get<List<OrderItemModel>>("OrderItems") ?? new();
                model.Order.Items = items;
                var provider = await _mediator.Send(new GetProviderByNameQuery() { ProviderName = model.SelectedProviderName });
                model.Order.ProviderId = provider.Id;
                await _mediator.Send(new AddOrderCommand() { OrderModel = model.Order });
                TempData.Set("OrderItems", new List<OrderItemModel>());
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Delete(int Id)
        {
            await _mediator.Send(new DeleteOrderCommand() { OrderId = Id });

            return RedirectToAction(nameof(Index));
        }
    }
}
