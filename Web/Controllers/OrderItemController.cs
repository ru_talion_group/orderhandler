﻿using Application.Models;
using Microsoft.AspNetCore.Mvc;
using Web.Extensions;

namespace Web.Controllers
{
    public class OrderItemController : Controller
    {
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(OrderItemModel orderItemModel)
        {
            if (ModelState.IsValid)
            {
                var orderItems = TempData.Get<List<OrderItemModel>>("OrderItems") ?? new();
                orderItems.Add(orderItemModel);
                TempData.Set("OrderItems", orderItems);
                return RedirectToAction("Create", "Orders");
            }
            return View(orderItemModel);
        }
        public IActionResult Details(Guid id)
        {
            var orderItems = TempData.Peek<List<OrderItemModel>>("OrderItems");
            var orderItem = orderItems?.FirstOrDefault(x => x.Id == id);

            if (orderItem == null)
            {
                return NotFound();
            }
            return View(orderItem);
        }

        public IActionResult Edit(Guid id)
        {
            var orderItems = TempData.Peek<List<OrderItemModel>>("OrderItems") ?? new();
            var orderItem = orderItems.FirstOrDefault(item => item.Id == id);

            if (orderItem == null)
            {
                return NotFound();
            }

            return View(orderItem);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Guid id, OrderItemModel orderItemModel)
        {
            if (ModelState.IsValid)
            {
                var orderItems = TempData.Get<List<OrderItemModel>>("OrderItems") ?? new();
                var orderItem = orderItems.FirstOrDefault(item => item.Id == id);
                var orderItemIndex = orderItems.IndexOf(orderItem);

                if (orderItem == null)
                {
                    return NotFound();
                }

                orderItems.Remove(orderItem);
                orderItems.Insert(orderItemIndex, orderItemModel);
                TempData.Set("OrderItems", orderItems);

                return RedirectToAction("Create", "Orders");
            }

            return View(orderItemModel);
        }

        public IActionResult Delete(Guid id)
        {
            var orderItems = TempData.Peek<List<OrderItemModel>>("OrderItems") ?? new();
            var orderItem = orderItems.FirstOrDefault(item => item.Id == id);

            if (orderItem == null)
            {
                return NotFound();
            }

            return View(orderItem);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(Guid id)
        {
            var orderItems = TempData.Get<List<OrderItemModel>>("OrderItems") ?? new();
            var orderItem = orderItems.FirstOrDefault(item => item.Id == id);
            
            if (orderItem == null)
            {
                return NotFound();
            }
            
            orderItems.Remove(orderItem);
            TempData.Set("OrderItems", orderItems);

            return RedirectToAction("Create", "Orders");
        }
    }
}
