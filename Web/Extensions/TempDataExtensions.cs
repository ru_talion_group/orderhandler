﻿using Microsoft.AspNetCore.Mvc.ViewFeatures;
using System.Text.Json;

namespace Web.Extensions
{
    public static class TempDataExtensions
    {
        public static void Set<T>(this ITempDataDictionary tempData, string key, T value) where T : class
        {
            tempData[key] = JsonSerializer.Serialize(value);
        }

        public static T? Get<T>(this ITempDataDictionary tempData, string key) where T : class
        {
            var item = tempData[key] as string;

            if (item == null)
            {
                return null;
            }

            return JsonSerializer.Deserialize<T>(item);
        }

        public static T? Peek<T>(this ITempDataDictionary tempData, string key) where T : class
        {
            var item = tempData.Peek(key) as string;

            if (item == null)
            {
                return null;
            }

            return JsonSerializer.Deserialize<T>(item);
        }
    }
}
