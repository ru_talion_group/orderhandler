﻿
using Application.Abstract;
using Application.Services.AutoMapper;
using Infrastructure.Repositories;

namespace Web.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection RegisterServices(this IServiceCollection services)
        {
            // Add services to the container.
            services.AddControllersWithViews();
            // Registration "MediatR".
            services.AddMediatR(cfg => {
                cfg.RegisterServicesFromAssemblies(AppDomain.CurrentDomain.GetAssemblies());
            });

            // Service registration "AutoMapper" for further injection.
            services.AddAutoMapper(typeof(AppMappingProfile));

            // Registering services with a limited lifetime.
            services.AddScoped<IProvidersRepository, ProvidersRepository>();
            services.AddScoped<IOrdersRepository, OrdersRepository>();

            return services;
        }
    }
}
