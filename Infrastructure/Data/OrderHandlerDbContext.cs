﻿using Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Serilog;
using Microsoft.Extensions.Logging;

namespace Infrastructure.Data
{
    public class OrderHandlerDbContext : DbContext
    {
        public OrderHandlerDbContext(DbContextOptions<OrderHandlerDbContext> options) : base(options) { }
     
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<Provider> Providers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.LogTo(Log.Error, LogLevel.Error);
        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Provider>()
                .HasIndex(u => u.Name)
                .IsUnique();

            builder.Entity<Provider>().HasData(new List<Provider>
            {
                new() {Id = 1, Name = "provider-1", Created = DateTime.UtcNow }
            });
        }
    }
}
