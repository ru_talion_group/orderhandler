﻿using Application.Abstract;
using Application.Models;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Infrastructure.Data;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories
{
    public class ProvidersRepository : IProvidersRepository
    {
        private readonly IDbContextFactory<OrderHandlerDbContext> _dbContextFactory;
        private readonly IMapper _mapper;

        public ProvidersRepository(IDbContextFactory<OrderHandlerDbContext> dbContextFactory, IMapper mapper)
        {
            _dbContextFactory = dbContextFactory;
            _mapper = mapper;
        }

        public async Task AddProvider(string providerName)
        {
            using var dbContext = await _dbContextFactory.CreateDbContextAsync();

            await dbContext.Providers.AddAsync(new Domain.Entity.Provider() { Name = providerName });
            await dbContext.SaveChangesAsync();
        }

        public async Task<IReadOnlyList<ProviderModel>> GetAllProviders()
        {
            using var dbContext = await _dbContextFactory.CreateDbContextAsync();

            return await dbContext.Providers
                .Include(x => x.Orders)
                .ProjectTo<ProviderModel>(_mapper.ConfigurationProvider)
                .AsNoTracking()
                .ToListAsync();
        }
        public async Task<ProviderModel> GetProvider(int id)
        {
            using var dbContext = await _dbContextFactory.CreateDbContextAsync();

            return await dbContext.Providers
                .Include(x => x.Orders)
                .ProjectTo<ProviderModel>(_mapper.ConfigurationProvider)
                .FirstOrDefaultAsync(x => x.Id == id)
                ?? throw new Exception("The user to delete was not found in the database");
                
        }
        public async Task<ProviderModel> GetProvider(string providerName)
        {
            using var dbContext = await _dbContextFactory.CreateDbContextAsync();

            return await dbContext.Providers
                .Include(x => x.Orders)
                .ProjectTo<ProviderModel>(_mapper.ConfigurationProvider)
                .FirstOrDefaultAsync(x => x.Name == providerName)
                ?? throw new Exception("The user to delete was not found in the database");

        }
        public async Task EditProvider(ProviderModel provider)
        {
            using var dbContext = await _dbContextFactory.CreateDbContextAsync();

            var providerFromDd = await dbContext.Providers.FirstOrDefaultAsync(x => x.Id == provider.Id)
                ?? throw new Exception("The user to edit was not found in the database");

            providerFromDd.Name = provider.Name;

            dbContext.Providers.Update(providerFromDd);
            await dbContext.SaveChangesAsync();
        }
        public async Task DeleteProvider(int providerId)
        {
            using var dbContext = await _dbContextFactory.CreateDbContextAsync();

            var provider = await dbContext.Providers.FirstOrDefaultAsync(x => x.Id == providerId) 
                ?? throw new Exception("The user to delete was not found in the database");
         
            dbContext.Providers.Remove(provider);
            await dbContext.SaveChangesAsync(); 
        }
    }
}
