﻿using Application.Abstract;
using Application.Models;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Domain.Entity;
using Infrastructure.Data;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories
{
    public class OrdersRepository : IOrdersRepository
    {
        private readonly IDbContextFactory<OrderHandlerDbContext> _dbContextFactory;
        private readonly IMapper _mapper;

        public OrdersRepository(IDbContextFactory<OrderHandlerDbContext> dbContextFactory, IMapper mapper)
        {
            _dbContextFactory = dbContextFactory;
            _mapper = mapper;
        }
        public async Task AddOrder(OrderModel orderModel)
        {
            using var dbContext = await _dbContextFactory.CreateDbContextAsync();
            var orderDomainModel = _mapper.Map<OrderModel, Order>(orderModel);
            dbContext.Orders.Add(orderDomainModel);
            await dbContext.SaveChangesAsync();

        }
        public async Task DeleteOrder(int orderId)
        {
            using var dbContext = await _dbContextFactory.CreateDbContextAsync();
            var orderDomainModel = await dbContext.Orders
                .Include(x => x.Items)
                .FirstOrDefaultAsync(x => x.Id == orderId)
                ?? throw new Exception("The order was not found in the database");
            dbContext.Orders.Remove(orderDomainModel);
            await dbContext.SaveChangesAsync();

        }
        public async Task<IReadOnlyList<OrderModel>> GetOrders(DateTime periodStart, DateTime periodEnd)
        {
            using var dbContext = await _dbContextFactory.CreateDbContextAsync();
            return await dbContext.Orders
                .Where(x => x.Created > periodStart & x.Created < periodEnd)
                .ProjectTo<OrderModel>(_mapper.ConfigurationProvider)
                .ToListAsync();
        }
        public async Task<OrderModel> GetOrder(int orderId)
        {
            using var dbContext = await _dbContextFactory.CreateDbContextAsync();
            return await dbContext.Orders
                .ProjectTo<OrderModel>(_mapper.ConfigurationProvider)
                .FirstOrDefaultAsync(x => x.Id == orderId)
                ?? throw new Exception("The order was not found in the database");
        }
    }
}
