﻿using Application.Abstract;
using Application.Models;
using MediatR;

namespace Application.Features.Orders.Queries
{
    public class GetOrdersQuery : IRequest<IReadOnlyList<OrderModel>>
    {
        public required DateTime PeriodStart { get; set; }
        public required DateTime PeriodEnd { get; set; }

        public class GetOrdersQueryHandler : IRequestHandler<GetOrdersQuery, IReadOnlyList<OrderModel>>
        {
            private readonly IOrdersRepository _ordersRepository;

            public GetOrdersQueryHandler(IOrdersRepository ordersRepository)
            {
                _ordersRepository = ordersRepository;
            }

            public async Task<IReadOnlyList<OrderModel>> Handle(GetOrdersQuery request, CancellationToken cancellationToken)
            {
                return await _ordersRepository.GetOrders(
                   request.PeriodStart == DateTime.MinValue ? DateTime.UtcNow.AddMonths(-1) : request.PeriodStart,
                   request.PeriodEnd == DateTime.MinValue ? DateTime.UtcNow : request.PeriodEnd);
            }
        }

    }
}
