﻿using Application.Abstract;
using Application.Models;
using MediatR;

namespace Application.Features.Orders.Queries
{
    public class GetOrderQuery : IRequest<OrderModel>
    {
        public int OrderId { get; set; }    

        public class GetOrderQueryHandler : IRequestHandler<GetOrderQuery, OrderModel>
        {
            private readonly IOrdersRepository _ordersRepository;

            public GetOrderQueryHandler(IOrdersRepository ordersRepository)
            {
                _ordersRepository = ordersRepository;
            }

            public async Task<OrderModel> Handle(GetOrderQuery request, CancellationToken cancellationToken)
            {
                return await _ordersRepository.GetOrder(request.OrderId);
            }
        }
    }
}
