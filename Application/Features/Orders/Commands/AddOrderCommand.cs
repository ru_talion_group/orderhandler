﻿using Application.Abstract;
using Application.Models;
using MediatR;

namespace Application.Features.Orders.Commands
{
    public class AddOrderCommand : IRequest<Unit>
    {
        public required OrderModel OrderModel { get; set; }

        public class AddOrderCommandHandler : IRequestHandler<AddOrderCommand, Unit> 
        {
            private readonly IOrdersRepository _ordersRepository;

            public AddOrderCommandHandler(IOrdersRepository ordersRepository)
            {
                _ordersRepository = ordersRepository;
            }

            public async Task<Unit> Handle(AddOrderCommand request, CancellationToken cancellationToken)
            {
                await _ordersRepository.AddOrder(request.OrderModel);
                return Unit.Value;
            }
        }    
    }
}
