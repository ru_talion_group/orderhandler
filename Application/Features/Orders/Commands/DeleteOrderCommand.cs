﻿using Application.Abstract;
using MediatR;

namespace Application.Features.Orders.Commands
{
    public class DeleteOrderCommand : IRequest<Unit>
    {
        public int OrderId { get; set; }

        public class DeleteOrderCommandHandler : IRequestHandler<DeleteOrderCommand, Unit>
        {
            private readonly IOrdersRepository _ordersRepository;

            public DeleteOrderCommandHandler(IOrdersRepository ordersRepository)
            {
                _ordersRepository = ordersRepository;
            }

            public async Task<Unit> Handle(DeleteOrderCommand request, CancellationToken cancellationToken)
            {
                await _ordersRepository.DeleteOrder(request.OrderId);
                return Unit.Value;
            }
        }

    }
}
