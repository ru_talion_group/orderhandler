﻿using Application.Abstract;
using MediatR;

namespace Application.Features.Providers.Commands
{
    public class DeleteProviderCommand : IRequest<Unit>
    {
        public required int ProviderId { get; set; }

        public class DeleteProviderCommandHandler : IRequestHandler<DeleteProviderCommand, Unit>
        {
            private readonly IProvidersRepository _providersRepository;

            public DeleteProviderCommandHandler(IProvidersRepository providersRepository)
            {
                _providersRepository = providersRepository;
            }

            public async Task<Unit> Handle(DeleteProviderCommand request, CancellationToken cancellationToken)
            {
                await _providersRepository.DeleteProvider(request.ProviderId);
                return Unit.Value;
            }
        }

    }
}
