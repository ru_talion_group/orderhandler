﻿using Application.Abstract;
using MediatR;

namespace Application.Features.Providers.Commands
{
    public class AddProviderCommand : IRequest<Unit>
    {
        public required string ProviderName { get; set; }
        public class AddProviderCommandHandler : IRequestHandler<AddProviderCommand, Unit>
        {
            private readonly IProvidersRepository _providersRepository;

            public AddProviderCommandHandler(IProvidersRepository providersRepository)
            {
                _providersRepository = providersRepository;
            }

            public async Task<Unit> Handle(AddProviderCommand request, CancellationToken cancellationToken)
            {
                await _providersRepository.AddProvider(request.ProviderName);
                return Unit.Value;
            }
        }
    }
}
