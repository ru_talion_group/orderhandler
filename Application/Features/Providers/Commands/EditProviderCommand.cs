﻿
using Application.Abstract;
using Application.Models;
using MediatR;

namespace Application.Features.Providers.Commands
{
    public class EditProviderCommand : IRequest<Unit>
    {
        public required ProviderModel Provider { get; set; }

        public class EditProviderCommandHandler : IRequestHandler<EditProviderCommand, Unit>
        {
            private readonly IProvidersRepository _providersRepository;

            public EditProviderCommandHandler(IProvidersRepository providersRepository)
            {
                _providersRepository = providersRepository;
            }

            public async Task<Unit> Handle(EditProviderCommand request, CancellationToken cancellationToken)
            {
                await _providersRepository.EditProvider(request.Provider);
                return Unit.Value;
            }



        }

    }
}
