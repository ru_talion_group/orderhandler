﻿using Application.Abstract;
using Application.Models;
using MediatR;
using Serilog;

namespace Application.Features.Providers.Queries
{
    public class GetAllProvidersQuery : IRequest<IReadOnlyList<ProviderModel>>
    {
        public class GetAllProvidersQueryHandler : IRequestHandler<GetAllProvidersQuery, IReadOnlyList<ProviderModel>>
        {
            private readonly IProvidersRepository _providersRepository;
            public GetAllProvidersQueryHandler(IProvidersRepository providersRepository)
            {
                _providersRepository = providersRepository;
            }
            public async Task<IReadOnlyList<ProviderModel>> Handle(GetAllProvidersQuery request, CancellationToken cancellationToken)
            {
                try
                {
                    return await _providersRepository.GetAllProviders();
                }
                catch (Exception ex)
                {
                    Log.Error(ex.ToString());
                    throw ex;
                }
            }
        }
    }
}
