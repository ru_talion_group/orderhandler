﻿using Application.Abstract;
using Application.Models;
using MediatR;

namespace Application.Features.Providers.Queries
{
    public class GetProviderByNameQuery : IRequest<ProviderModel>
    {
        public required string ProviderName { get; set; }
        public class GetProviderQueryHandler : IRequestHandler<GetProviderByNameQuery, ProviderModel>
        {
            private readonly IProvidersRepository _providersRepository;
            public GetProviderQueryHandler(IProvidersRepository providersRepository)
            {
                _providersRepository = providersRepository;
            }
            public async Task<ProviderModel> Handle(GetProviderByNameQuery request, CancellationToken cancellationToken)
            {
                return await _providersRepository.GetProvider(request.ProviderName);

            }
        }
    }
}
