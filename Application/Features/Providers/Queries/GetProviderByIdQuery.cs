﻿using Application.Abstract;
using Application.Models;
using MediatR;

namespace Application.Features.Providers.Queries
{
    public class GetProviderByIdQuery : IRequest<ProviderModel>
    {
        public required int ProviderId { get; set; }
        public class GetProviderQueryHandler : IRequestHandler<GetProviderByIdQuery, ProviderModel>
        {
            private readonly IProvidersRepository _providersRepository;
            public GetProviderQueryHandler(IProvidersRepository providersRepository)
            {
                _providersRepository = providersRepository;
            }
            public async Task<ProviderModel> Handle(GetProviderByIdQuery request, CancellationToken cancellationToken)
            {
                return await _providersRepository.GetProvider(request.ProviderId);
            }
        }
    }
}
