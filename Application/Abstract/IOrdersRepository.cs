﻿using Application.Models;

namespace Application.Abstract
{
    public interface IOrdersRepository
    {
        Task<IReadOnlyList<OrderModel>> GetOrders(DateTime periodStart, DateTime periodEnd);
        Task<OrderModel> GetOrder(int orderId);
        Task AddOrder(OrderModel orderModel);
        Task DeleteOrder(int orderId);
    }
}