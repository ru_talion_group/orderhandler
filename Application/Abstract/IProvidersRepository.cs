﻿using Application.Models;

namespace Application.Abstract
{
    public interface IProvidersRepository
    {
        Task<IReadOnlyList<ProviderModel>> GetAllProviders();
        Task AddProvider(string providerName);
        Task<ProviderModel> GetProvider(int id);
        Task<ProviderModel> GetProvider(string providerName);
        Task DeleteProvider(int providerId);
        Task EditProvider(ProviderModel provider);
    }
}