﻿namespace Application.Models
{
    public class OrderItemModel
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public required string Name { get; set; }
        public required decimal Quantity { get; set; }
        public required string Unit { get; set; }
    }
}
