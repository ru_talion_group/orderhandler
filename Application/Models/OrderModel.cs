﻿using Domain.Entity;

namespace Application.Models
{
    public class OrderModel
    {
        public int Id { get; set; } 
        public string? Number { get; set; }
        public int ProviderId { get; set; }
        public ProviderModel? Provider { get; set; }
        public DateTime Created { get; set; } = DateTime.UtcNow;
        public List<OrderItemModel> Items { get; set; } = new List<OrderItemModel>();
    }
}
