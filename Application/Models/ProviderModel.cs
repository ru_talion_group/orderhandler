﻿using Domain.Entity;

namespace Application.Models
{
    public class ProviderModel
    {
        public int Id { get; set; }
        public required string Name { get; set; }
        public DateTime Created { get; set; } = DateTime.UtcNow;
        public List<OrderModel> Orders { get; set; } = new List<OrderModel>();
    }
}
