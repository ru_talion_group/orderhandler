﻿using Application.Models;
using AutoMapper;
using Domain.Entity;

namespace Application.Services.AutoMapper
{
    public class AppMappingProfile : Profile
    {
        public AppMappingProfile()
        {
            CreateMap<ProviderModel, Provider>();
            CreateMap<Provider, ProviderModel>();

            CreateMap<Order, OrderModel>();
            CreateMap<OrderModel, Order>();

            CreateMap<OrderItem, OrderItemModel>();
            CreateMap<OrderItemModel, OrderItem>();
        }
    }
}
